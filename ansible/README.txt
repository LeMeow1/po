Quels sont les fichiers ?
Un playbook: cisco.yml
Un inventaire: inventory
Un fichier host de l'inventaires: hosts
Des hooks git : .git/hooks/pre-commit et post-commit
Des confs : switch.j2 et router.j2

Comment ca marche ?
Le playbook va chercher les cibles dans le fichier hosts de l'inventaire.
Il va egalement chercher le fichier de config contenant les commandes cisco.
Le fichier de config utilise des variables definies dans le fichier hosts.
A chaque commit git, il y aura un test du playbook ansible, si c'est bon ça commit et ça déploit sinon ça empeche le commit

Coment gerer sa conf ?
Il est possible de passer toutes les commandes dans le playbook directement mais ca devient vite sale et illisible.
Pour eviter ca on peut sourcer la conf grace a la commande "src:" du module "ios_conf", il faut juste pointer vers le fichier de conf en .cfg.
Encore mieux on peut faire en sorte que ce fichier de conf soit un template qui sera complete d'une maniere differente pour chaque hote.
Ce template sera en .j2 et contiendra toutes les commandes a passer a l'equipement, et fera appel a des variables de l'inventaire pour completer les commandes.
    ex : "hostname {{ hostname}}", executera la commande "hostname Switch_1" car la variable "hostname" aura ete definie dans l'inventaire pour le switch 1 comme etant "Switch_1".

A implementer :
	- Fournir l'inventaire de variables et de nouveaux equipements
	- Faire des confs correctes pour les équipements
	- Toutes les autres choses sympa auxquelles j'ai pas pense
