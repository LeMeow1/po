# Projet_Opensource

Le projet consiste à déployer un réseau informatique automatiquement.
Le but est de push des configs de switchs et/ou routeurs via ansible.
Dans un second temps mettre un place des snmp sur ces équipements qui renverrons des infos à un serveur Nagios également déployer automatiquement.

![](Schéma_de_principe.png)


Inventaire technologique :
- eve-ng 2.0.6-11
-- 3 switchs I86BI_LINUXL2-ADVENTERPRISEK9-M v15.2
- nagios 4.4.5
- ansible 2.9.1
- module ios_config
- python 3.8.0
- vagrant 2.2.6
- SNMP
- VMWare / VirtualBox 6.0.14

adressage ip :
- hote	192.168.50.1
- eve	192.168.50.2
- s1	192.168.50.101
- s2	192.168.50.102
- s3	192.168.50.103
- nagios	192.168.50.

configuration :
- vlan, subnet, etc ...

Objectif : 
A partir d'ansible, le nagios et les switchs doivent être configuré. De plus, le nagios doit être créé à l'aide de vagrant.
Le nagios doit pouvoir monitorer à terme les switchs.


Procedure :
- Installation et configuration de eve-ng
- Ajout des switchs
- Interface de sortie dans eve pour les switch, afin de pouvoir les configurer à l'aide d'ansible
- Création d'un playbook
- Création de nagios à l'aide de vagrant
- Configuration des switch et de nagios avec ansible
- Vérification de la solution (nagios doit pouvoir monitorer les switchs


Gestion de la solution :
comment démarrer, arreter, sauvegarder, restaurer, monitorer la solution
•les procédures opérationnelles courantes : Les changements standards (Création/suppression d'une nouvelle instance de tel ou tel objet fonctionnel Exemple : ajout/suppression d'utilisateurs, ajout d'un LUN sur une baie)
•Le plan de production et l'odonnancement des traitements



